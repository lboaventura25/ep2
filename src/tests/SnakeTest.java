package tests;

import ep2.Snake;
import ep2.SnakeStar;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;

import static org.junit.jupiter.api.Assertions.*;

class SnakeStarTest {

    public String nome = "Star";

    private int tamanhoSnakeComum = 3;
    private int pontos = 0;

    private Snake snake;

    private ImageIcon snakeImagem;

    private ImageIcon cabecaDireita;
    private ImageIcon cabecaEsquerda;
    private ImageIcon cabecaCima;
    private ImageIcon cabecaBaixo;

    private int[] snakeTamanhoX = new int[750];
    private int[] snakeTamanhoY = new int[750];

    @BeforeEach
    public void preparaTestes() {
        snake = new SnakeStar();
    }

    @Test
    public void testaConstrutor() {
        //Verificação
        assertEquals(this.pontos, this.snake.getPontos());
        assertEquals(this.tamanhoSnakeComum, this.snake.getTamanhoSnake());
        assertEquals(this.nome, this.snake.getNome());
    }

    @Test
    public void testaGetSetCabecaBaixo() {
        //Ação
        this.cabecaBaixo = new ImageIcon("././assets/downmouth.png");
        this.snake.setCabecaBaixo(this.cabecaBaixo);

        //Verificação
        assertEquals(this.cabecaBaixo, this.snake.getCabecaBaixo());
    }

    @Test
    public void testaGetSetCabecaCima() {
        //Ação
        this.cabecaCima = new ImageIcon("././assets/upmouth.png");
        this.snake.setCabecaCima(this.cabecaCima);

        //Verificação
        assertEquals(this.cabecaCima, this.snake.getCabecaCima());
    }

    @Test
    public void testaGetSetCabecaEsquerda() {
        //Ação
        this.cabecaEsquerda = new ImageIcon("././assets/leftmouth.png");
        this.snake.setCabecaEsquerda(this.cabecaEsquerda);

        //Verificação
        assertEquals(this.cabecaEsquerda, this.snake.getCabecaEsquerda());
    }

    @Test
    public void testaGetSetCabecaDireita() {
        //Ação
        this.cabecaDireita = new ImageIcon("././assets/rightmouth.png");
        this.snake.setCabecaCima(this.cabecaCima);

        //Verificação
        assertEquals(this.cabecaBaixo, this.snake.getCabecaDireita());
    }

    @Test
    public void testaGetSetPontos() {
        //Ação
        this.pontos = 4;
        this.snake.setPontos(this.pontos);

        //Verificação
        assertEquals((this.pontos * 2), this.snake.getPontos());
    }

    @Test
    public void testaGetSetTamanhoSnake() {
        //Ação
        this.tamanhoSnakeComum = 2;
        this.snake.setTamanhoSnake(this.tamanhoSnakeComum);

        //Verificação
        assertEquals((this.tamanhoSnakeComum + 3), this.snake.getTamanhoSnake());
    }

    @Test
    public void testaGetSetNome() {
        //Ação
        this.snake.setNome(this.nome);

        //Verificação
        assertEquals(this.nome, this.snake.getNome());
    }

    @Test
    public void testaResetTamanhoSnake() {
        //Ação
        this.snake.setTamanhoSnake(this.tamanhoSnakeComum);
        this.snake.resetTamanhoSnake();

        //Verificação
        assertEquals(this.tamanhoSnakeComum, this.snake.getTamanhoSnake());
    }

    @Test
    public void testaResetPontos() {
        //Ação
        this.snake.setPontos(this.pontos);
        this.snake.resetPontos();

        //Verificação
        assertEquals(0, this.snake.getPontos());
    }

    @Test
    public void testaGetSetSnakeImagem() {
        //Ação
        this.snakeImagem = new ImageIcon("././assets/snakeimage.png");
        this.snake.setSnakeImagem(this.snakeImagem);

        //Verificação
        assertEquals(this.snakeImagem, this.snake.getSnakeImagem());
    }

    @Test
    public void testaGetSnakeTamanhoX() {
        //Ação
        this.snakeTamanhoX[2] = 50;
        this.snakeTamanhoX[1] = 75;
        this.snakeTamanhoX[0] = 100;

        this.snakeTamanhoY[2] = 100;
        this.snakeTamanhoY[1] = 100;
        this.snakeTamanhoY[0] = 100;

        //Verificação
        assertArrayEquals(this.snakeTamanhoX, this.snake.getSnakeTamanhoX());
    }

    @Test
    public void testaGetSnakeTamanhoY() {
        //Ação
        this.snakeTamanhoX[2] = 50;
        this.snakeTamanhoX[1] = 75;
        this.snakeTamanhoX[0] = 100;

        this.snakeTamanhoY[2] = 100;
        this.snakeTamanhoY[1] = 100;
        this.snakeTamanhoY[0] = 100;

        //Verificação
        assertArrayEquals(this.snakeTamanhoY, this.snake.getSnakeTamanhoY());
    }

    @AfterEach
    public void depoisTestes() {
        snake = null;
    }
}