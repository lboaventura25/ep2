package tests;

import ep2.Fruto;
import ep2.SimpleFruit;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;

import static org.junit.jupiter.api.Assertions.*;

class SimpleFruitTest {

    private Fruto fruto;

    private int pontos = 1;
    private int acrescentaTamanho = 1;

    private ImageIcon frutoImagem;

    private int[] frutoXPos = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325,
            350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650,
            675, 700, 725, 750, 775, 800, 825, 850};

    private int[] frutoYPos = {75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325,
            350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625};


    @BeforeEach
    public void preparaTestes() {
        //Preparação
        fruto = new SimpleFruit();
    }

    @Test
    public void testaConstrutor() {
        //Ação
         this.frutoImagem= new ImageIcon("././assets/SimpleFruit.png");

         //Verificação
        assertEquals(this.pontos, this.fruto.getPontos());
        assertEquals(this.acrescentaTamanho, this.fruto.getAcrescentaTamanho());
        assertEquals(this.frutoImagem.getDescription(), this.fruto.getFrutoImagem().getDescription());
    }

    @Test
    void testaGetSetAcrescentaTamanho() {
        //Ação
        this.fruto.setAcrescentaTamanho(this.acrescentaTamanho);

        //Verificação
        assertEquals(this.acrescentaTamanho, this.fruto.getAcrescentaTamanho());
    }

    @Test
    void testaGetSetPontos() {
        //Ação
        this.fruto.setPontos(this.pontos);

        //Verificação
        assertEquals(this.pontos, this.fruto.getPontos());
    }

    @Test
    public void testaGetSetFrutoImagem() {
        //Ação
        this.fruto.setFrutoImagem(this.frutoImagem);

        //Verificação
        assertEquals(this.frutoImagem, this.fruto.getFrutoImagem());
    }

    @Test
    public void testaGetFrutoXPos() {
        //Verificação
        assertArrayEquals(this.frutoXPos, this.fruto.getFrutoXPos());
    }

    @Test
    public void testaGetFrutoYPos() {
        //Verificação
        assertArrayEquals(this.frutoYPos, this.fruto.getFrutoYPos());
    }

    @Test
    public void testaGetXPos() {
        //Verificação
        assertArrayEquals(this.frutoXPos, this.fruto.getFrutoXPos());
    }

    @Test
    public void testaGetYPos() {
        //Verificação
        assertArrayEquals(this.frutoYPos, this.fruto.getFrutoYPos());
    }


    @AfterEach
    public void depoisTestes() {
        this.fruto = null;
    }
}